package com.example.ref;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
	private static final Logger LOGGER = LoggerFactory.getLogger(App.class.getName());

	public static void main(String[] args) {
		ServiceX service = new ServiceX();
		String jsonStr = "";

		ValueType value;
		try {
			value = service.createValidValueType(jsonStr, "06");
			LOGGER.info(value.toString());
		} catch (ServiceException e) {
			LOGGER.error("An error occoured", e);
		}

	}

}
