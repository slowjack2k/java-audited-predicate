package com.example.ref;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ServiceX {
	private static final String CODE_03 = "09";
	private static final String CODE_01 = "01";
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceX.class.getName());

	public ValueType createValidValueType(String serializedData, String code06) throws ServiceException {

		ValueType data = parseData(serializedData);

		if (data != null) {

			if (StringUtils.equals(data.getCode_01(), CODE_01)) {

				if (StringUtils.equals(data.getCode_03(), CODE_03)) {

					if (StringUtils.isNotBlank(code06)) {

						if (!code06.equals(data.getCode_06())) {

							throw new ServiceException("Code_06 does not match");

						}

						return data;

					}

				} else {

					LOGGER.info("Code_03 {} does not match {}", data.getCode_03(), CODE_03);

				}

			} else {

				LOGGER.info("Code_01 {} does not match {}", data.getCode_01(), CODE_01);

			}

		} else {

			LOGGER.info("No data parsed");

		}

		return null;

	}

	private ValueType parseData(String serializedData) {
		ObjectMapper mapper = new ObjectMapper();
		ValueType result = null;

		try {
			result = mapper.readValue(serializedData, ValueType.class);
		} catch (Exception e) {
			LOGGER.error("An errror occured", e);
		}

		return result;
	}
}
