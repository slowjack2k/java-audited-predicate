package com.example.ref;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@ToString(includeFieldNames = true)
public class ValueType {

	@NonNull
	private String code_01;
	@NonNull
	private String code_03;
	@NonNull
	private String code_06;

}
