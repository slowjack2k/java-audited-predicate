package com.example.ref.specs

import com.example.ref.ServiceException
import com.example.ref.ServiceX
import com.example.ref.ValueType

import spock.lang.*

class ServiceXSpecification extends Specification {
	def ServiceX service = null

	def setup() {
		service = new ServiceX()
	}

	def "it creates a ValueType object, when all parameters are valid"() {
		expect:
		service.createValidValueType(json, code06) == new ValueType("01", "09", "4")

		where:
		json = '{"code_01": "01", "code_03": "09", "code_06": "4"}'
		code06 = '4'
	}

	def "it does not create a ValueType when code06 is null"() {
		expect:
		service.createValidValueType(json, null) == null

		where:
		json = '{"code_01": "02", "code_03": "09", "code_06": "4"}'
		code06 = '4'
	}

	def "it does not create a ValueType when code_01 doesn't match '01'"() {
		expect:
		service.createValidValueType(json, code06) == null

		where:
		json = '{"code_01": "02", "code_03": "09", "code_06": "4"}'
		code06 = '4'
	}

	def "it does not create a ValueType when code_03 doesn't match '09'"() {
		expect:
		service.createValidValueType(json, code06) == null

		where:
		json = '{"code_01": "01", "code_03": "08", "code_06": "4"}'
		code06 = '4'
	}

	def "it throws an exception when code_06 doesn't match \$code06"() {
		expect:

		try {
			service.createValidValueType(json, code06)
			assert false
		} catch (ServiceException ex){
			assert ex.message == "Code_06 does not match"
		}

		where:
		json = '{"code_01": "01", "code_03": "09", "code_06": "3"}'
		code06 = '4'
	}
}
